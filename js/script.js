let userArr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

let addList = function (arr, itemDom= document.body) {
    const list = document.createElement('ul');
    const listItems = arr.map(item => `<li>${item}</li>`);
    console.log(listItems)
    const listItemsHtml = listItems.join('');
    list.innerHTML = listItemsHtml;
    console.log(listItemsHtml)
    itemDom.append(list);
}

addList(userArr)""